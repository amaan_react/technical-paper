# <center>**Object-Oriented Programming and its Concepts**</center>
## <center>**Mohd Amaan Ali**</center>
### <center>University of Pune, India</center>
***
<br/><br/>

## **Introduction**
Object-oriented programming System(OOPs) is a programming paradigm based on the concept of “objects” that contain data and methods. The primary purpose of object-oriented programming is to increase the flexibility and maintainability of programs. Object oriented programming brings together data and its behaviour(methods) in a single location(object) makes it easier to understand how a program works. 
<br/><br/>
![](java-oops.png)
<br/><br/>

## **OOPs Concepts – Table of Contents**
1. What is an Object
2. What is a Class
3. Inheritance
4. Polymorphism
5. Abstraction
6. Encapsulation
<br/><br/>

## **What is an Object**

It is a basic unit of Object Oriented Programming and represents the real life entities. A typical Java program creates many objects, which as you know, interact by invoking methods. An object consists of:

1. **State**: It is represented by attributes of an object. It also reflects the properties of an object.

2. **Behavior**: It is represented by methods of an object. It also reflects the response of an object with other objects.

3. **Identity**: It gives a unique name to an object and enables one object to interact with other objects.

**Example**:

**Object**: House

**State**: Address, Color, Area

**Behavior**: Open door, close door

So if I had to write a class based on states and behaviours of House. I can do it like this: States can be represented as instance variables and behaviours as methods of the class. We will see how to create classes in the next section of this guide.
```java
class House {
   String address;
   String color;
   double are;
   void openDoor() {
      //Write code here
   }
   void closeDoor() {
      //Write code here
   }
 ...
 ...
}
```
<br/><br/>

## **What is a Class**
A class is a user defined blueprint or prototype from which objects are created. It represents the set of properties or methods that are common to all objects of one type. In general, class declarations can include these components, in order:

1. **Modifiers**: A class can be public or has default access (Refer this for details).

2. **Class name**: The name should begin with a initial letter (capitalized by convention).

3. **Superclass**(if any): The name of the class’s parent (superclass), if any, preceded by the keyword extends. A class can only extend (subclass) one parent.

4. **Interfaces**(if any): A comma-separated list of interfaces implemented by the class, if any, preceded by the keyword implements. A class can implement more than one interface.

5. **Body**: The class body surrounded by braces, { }.

**Example**:

Here we have a class Website that has two data members (also known as fields, instance variables and object states). This is just a blueprint, it does not represent any website, however using this we can create Website objects (or instances) that represents the websites. We have created two objects, while creating objects we provided separate properties to the objects using constructor.
```java
public class Website {
   //fields (or instance variable)
   String webName;
   int webAge;

   // constructor
   Website(String name, int age){
      this.webName = name;
      this.webAge = age;
   }
   public static void main(String args[]){
      //Creating objects
      Website obj1 = new Website("beginnersbook", 5);
      Website obj2 = new Website("google", 18);

     //Accessing object data through reference
     System.out.println(obj1.webName+" "+obj1.webAge);
     System.out.println(obj2.webName+" "+obj2.webAge);
   }

```
**Output**:
```
beginnersbook 5
google 18
```
<br/><br/>

## **Inheritance**
Inheritance is an important pillar of OOP(Object Oriented Programming). It is the mechanism in java by which one class is allow to inherit the features(fields and methods) of another class.

**Important terminology**:

1. **Super Class**: The class whose features are inherited is known as superclass(or a base class or a parent class).

2. **Sub Class**: The class that inherits the other class is known as subclass(or a derived class, extended class, or child class). The subclass can add its own fields and methods in addition to the superclass fields and methods.

3. **Reusability**: Inheritance supports the concept of “reusability”, i.e. when we want to create a new class and there is already a class that includes some of the code that we want, we can derive our new class from the existing class. By doing this, we are reusing the fields and methods of the existing class.

The keyword used for inheritance is **extends**.

**Syntax**:
```java
class derived-class extends base-class  
{  
   //methods and fields  
}  
```
**Example**:

In this example, we have a parent class Teacher and a child class MathTeacher. In the MathTeacher class we need not to write the same code which is already present in the present class. Here we have college name, designation and does() method that is common for all the teachers, thus MathTeacher class does not need to write this code, the common data members and methods can inherited from the Teacher class.
```java
class Teacher {
   String designation = "Teacher";
   String college = "Beginnersbook";
   void does(){
	System.out.println("Teaching");
   }
}
public class MathTeacher extends Teacher{
   String mainSubject = "Maths";
   public static void main(String args[]){
      MathTeacher obj = new MathTeacher();
      System.out.println(obj.college);
      System.out.println(obj.designation);
      System.out.println(obj.mainSubject);
      obj.does();
   }
}
```
**Output**:
```
Beginnersbook
Teacher
Maths
Teaching
```

**Types of Inheritance:**

- **Single Inheritance:** refers to a child and parent class relationship where a class extends the another class.

- **Multilevel inheritance:** refers to a child and parent class relationship where a class extends the child class. For example class A extends class B and class B extends class C.

- **Hierarchical inheritance:** refers to a child and parent class relationship where more than one classes extends the same class. For example, class B extends class A and class C extends class A.

- **Multiple Inheritance:** refers to the concept of one class extending more than one classes, which means a child class has two parent classes. Java doesn’t support multiple inheritance.
<br/><br/>

## **Polymorphism**
Polymorphism refers to the ability of OOPs programming languages to differentiate between entities with the same name efficiently. This is done by Java with the help of the signature and declaration of these entities.

**Example**:
```java
// Java program to demonstrate Polymorphism 
  
// This class will contain 
// 3 methods with same name, 
// yet the program will 
// compile & run successfully 
public class Sum { 
  
    // Overloaded sum(). 
    // This sum takes two int parameters 
    public int sum(int x, int y) 
    { 
        return (x + y); 
    } 
  
    // Overloaded sum(). 
    // This sum takes three int parameters 
    public int sum(int x, int y, int z) 
    { 
        return (x + y + z); 
    } 
  
    // Overloaded sum(). 
    // This sum takes two double parameters 
    public double sum(double x, double y) 
    { 
        return (x + y); 
    } 
  
    // Driver code 
    public static void main(String args[]) 
    { 
        Sum s = new Sum(); 
        System.out.println(s.sum(10, 20)); 
        System.out.println(s.sum(10, 20, 30)); 
        System.out.println(s.sum(10.5, 20.5)); 
    } 
} 
```
**Output**:
```
30
60
31.0
```
**Types of Polymorphism**
1) Static Polymorphism
2) Dynamic Polymorphism

**Static Polymorphism**:
Polymorphism that is resolved during compiler time is known as static polymorphism. **Method overloading** is an example of compile time polymorphism.

- **Method Overloading**: This allows us to have more than one method having the same name, if the parameters of methods are different in number, sequence and data types of parameters. 

**Dynamic Polymorphism**:
It is also known as Dynamic Method Dispatch. Dynamic polymorphism is a process in which a call to an overridden method is resolved at runtime, thats why it is called runtime polymorphism. 

- **Method overriding**: In object-oriented programming, is a language feature that allows a subclass or child class to provide a specific implementation of a method that is already provided by one of its superclasses or parent classes.
<br/><br/>

## **Abstraction**
Data Abstraction is the property by virtue of which only the essential details are displayed to the user.The trivial or the non-essentials units are not displayed to the user. Ex: A car is viewed as a car rather than its individual components.
Data Abstraction may also be defined as the process of identifying only the required characteristics of an object ignoring the irrelevant details. The properties and behaviours of an object differentiate it from other objects of similar type and also help in classifying/grouping the objects.

**Example**:

Consider a man driving a car. The man only knows that pressing the accelerators will increase the speed of car or applying brakes will stop the car but he does not know about how on pressing the accelerator the speed is actually increasing, he does not know about the inner mechanism of the car or the implementation of accelerator, brakes etc in the car. This is what abstraction is.
<br/><br/>

## **Encapsulation**
Encapsulation is defined as the wrapping up of data under a single unit. It is the mechanism that binds together code and the data it manipulates. Another way to think about encapsulation is, it is a protective shield that prevents the data from being accessed by the code outside this shield.

- Technically in encapsulation, the variables or data of a class is hidden from any other class and can be accessed only through any member function of own class in which they are declared.

- As in encapsulation, the data in a class is hidden from other classes, so it is also known as data-hiding.

- Encapsulation can be achieved by Declaring all the variables in the class as private and writing public methods in the class to set and get the values of variables.

**Example**:

How to
1) Make the instance variables private so that they cannot be accessed directly from outside the class. You can only set and get values of these variables through the methods of the class.
2) Have getter and setter methods in the class to set and get the values of the fields.

```java
class EmployeeCount
{
   private int numOfEmployees = 0;
   public void setNoOfEmployees (int count)
   {
       numOfEmployees = count;
   }
   public double getNoOfEmployees () 
   {
       return numOfEmployees;
   }
}
public class EncapsulationExample
{
   public static void main(String args[])
   {
      EmployeeCount obj = new EmployeeCount ();
      obj.setNoOfEmployees(5613);
      System.out.println("No Of Employees: "+(int)obj.getNoOfEmployees());
    }
}
```
**Output**:
```
No Of Employees: 5613
```
The class EncapsulationExample that is using the Object of class EmployeeCount will not able to get the NoOfEmployees directly. It has to use the setter and getter methods of the same class to set and get the value.

<br/><br/>

## **References**
[1] https://www.geeksforgeeks.org/object-oriented-programming-oops-concept-in-java/

[2] https://beginnersbook.com/2013/04/oops-concepts/#8

[3] https://www.javatpoint.com/java-oops-concepts